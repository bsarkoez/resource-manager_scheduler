import socket
import threading

from resource_manager import ResourceManager
from scheduler import Scheduler
from utils import Job


class Server:
    def __init__(self, number_of_cores):
        self.host = '127.0.0.1'
        self.port = 6789
        self.socket = None
        self.resource_manager = ResourceManager(number_of_cores)
        self.scheduler = Scheduler(self.resource_manager)
        self.job_id_lock = threading.Lock()
        self.job_ids = 0
        self.running = True

    def start(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.host, self.port))
        self.socket.listen()
        while self.running:
            conn, addr = self.socket.accept()
            with conn:
                data = conn.recv(1024)
                print(data, flush=True)
                decoded_data = data.decode()

                if decoded_data == 'server-shutdown':
                    break

                if decoded_data == 'status':
                    status = self.resource_manager.get_status() + self.scheduler.get_status()
                    conn.sendall(str.encode(status))

                if 'submit' in decoded_data:
                    success = self.parse_and_submit_job_successfully(decoded_data)
                    if not success:
                        conn.sendall(b'job not successfully submitted')
                    else:
                        conn.sendall(b'job successfully submitted')

    def parse_and_submit_job_successfully(self, submit_str):
        given_job = submit_str.split('submit ')[1][1:-1] # additionally remove first [ and last ]

        self.job_id_lock.acquire()
        self.job_ids += 1
        job_id = self.job_ids
        self.job_id_lock.release()

        program = given_job.split(';')[1]
        threads = self.get_list_of_threads(given_job.split(';')[2])

        self.scheduler.add_job(Job(job_id, job_id, program, threads))
        return True

    @staticmethod
    def get_list_of_threads(threads_str):
        if '-' in threads_str:
            start = int(threads_str.split('-')[0])
            end = int(threads_str.split('-')[1])
            return list(range(start, end + 1))
        else:
            # enumeration with ,
            return list(map(int, threads_str.split(',')))

    def close(self):
        self.running = False
        socket.socket(socket.AF_INET,
                      socket.SOCK_STREAM).connect((self.host, self.port))
        self.socket.close()
