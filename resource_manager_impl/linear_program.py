import sys
import pulp


class LinearProgram:
    def __init__(self, running_times_dict, number_of_machines):
        self.running_times = running_times_dict

        self.number_of_jobs = len(running_times_dict.keys())
        self.number_of_machines = number_of_machines

    def start(self):
        expected_makespan, job_assignments = self.find_number_of_cores()
        print(expected_makespan)
        print(job_assignments)

        cmax, schedule = self.try_different_execution_orders(job_assignments)
        print(cmax, schedule)
        return cmax, schedule

    def try_different_execution_orders(self, job_assignments):
        # (job name, number of cores, list of assigned cores, approximate running time)

        min_cmax = sys.maxsize
        best_schedule = None

        orders = [job_assignments,
                  reversed(job_assignments),
                  sorted(job_assignments, key=lambda x: len(x[2]), reverse=True),
                  sorted(job_assignments, key=lambda x: len(x[2])),
                  sorted(job_assignments, key=lambda x: x[3]),
                  sorted(job_assignments, key=lambda x: x[3], reverse=True)]
        for o in orders:
            starting_times_machine_dict, jobs_with_starting_times = self.assign_starting_times(o)
            makespan = self.get_cmax(starting_times_machine_dict)
            if makespan < min_cmax:
                min_cmax = makespan
                best_schedule = jobs_with_starting_times

        return min_cmax, sorted(best_schedule, key=lambda x: x[-1]) # sort schedule according to starting time

    @staticmethod
    def get_cmax(machine_dict):
        cmax = 0
        for m in machine_dict:
            if len(machine_dict[m]) == 0:
                continue
            job_id, running_time, starting_time = machine_dict[m][-1]  # last entry
            total_time_last_job = running_time + starting_time
            if total_time_last_job > cmax:
                cmax = total_time_last_job

        return cmax

    def assign_starting_times(self, job_assignments):
        machine_dict = {}
        for i in range(self.number_of_machines):
            machine_dict[str(i)] = []

        jobs_with_starting_times = []

        for (job_id, num_cores, cores, running_time) in job_assignments:
            earliest_starting_time = self.get_max_total_time_of_cores(cores, machine_dict)
            for c in cores:
                machine_dict[str(c)].append((job_id, running_time, earliest_starting_time))
            jobs_with_starting_times.append((job_id, num_cores, cores, running_time, earliest_starting_time))

        return machine_dict, jobs_with_starting_times

    @staticmethod
    def get_max_total_time_of_cores(cores, machine_dict):
        end_times = []
        for c in cores:
            if len(machine_dict[str(c)]) == 0:
                end_times.append(0)
            else:
                job_id, running_time, starting_time = machine_dict[str(c)][-1] # last entry
                total_time_last_job = running_time + starting_time
                end_times.append(total_time_last_job)

        return max(end_times)

    def find_number_of_cores(self):
        problem = self.get_linear_program_problem()
        variables = [v for v in problem.variables() if v.varValue != 0]

        job_assignments = [('-'.join(v.name.split('_')[1:-1]), int(v.name.split('_')[-1]))
                           for v in variables if v.name.startswith('c_')]
        job_core_assignments = []
        make_span = [float(v.varValue) for v in variables if v.name == 'cmax'][0]

        # job_core_assignments: [(job name, number of cores, list of assigned cores, approximate running time)]

        for (n, c) in job_assignments:
            cores = [int(m.name.split('_')[-1])
                     for m in variables
                     if m.name.startswith('x_%s_%d_' % (n.replace('-', '_'), c))]
            job_core_assignments.append((n, c, cores, [t for (x, t) in self.running_times[n] if x == c][0]))

        return make_span, job_core_assignments

    @staticmethod
    def sort_job_core_assignments_urgency(assignment):
        return sorted(assignment, key=lambda x: len(x[2]), reverse=True)

    def get_linear_program_problem(self):
        core_assignment = [['%s-%d' % (job_id, c) for (c, r) in self.running_times[job_id]]
                           for job_id in self.running_times]
        core_assignment = [item for sublist in core_assignment for item in sublist]

        x_assignments = [['%s-%d-%d' % (job_id, c, machine_id) for (c, r) in self.running_times[job_id]] 
                         for job_id in self.running_times for machine_id in range(self.number_of_machines)]
        x_assignments = [item for sublist in x_assignments for item in sublist]

        makespan_minimization = pulp.LpProblem("Scheduling Problem", pulp.LpMinimize)

        c_max = pulp.LpVariable('cmax', lowBound=0, cat='Continuous')
        x = pulp.LpVariable.dicts('x', x_assignments, cat='Binary')
        cores = pulp.LpVariable.dicts('c', core_assignment, cat='Binary')

        # objective function
        makespan_minimization += c_max, 'Z'

        # constraints

        for i in self.running_times:
            # constraint: each job only executed with one specific number of cores 
            # (e.g. only 4 cores, not 2 AND 8 cores)
            makespan_minimization += sum(cores['%s-%d' % (i, c)] for (c, _) in self.running_times[i]) == 1

            # constraint: jobs executed with z cores should get z assignments of the form x[job_id-?-machine_id]
            for (c, r) in self.running_times[i]:
                makespan_minimization += sum(x['%s-%d-%d' % (i, c, j)] 
                                             for j in range(self.number_of_machines)) == cores['%s-%d' % (i, c)] * c

        for j in range(self.number_of_machines):
            # constraint: assignments * running_time <= c_max
            l = [[x['%s-%d-%d' % (i, c, j)] * r for (c, r) in self.running_times[i]] for i in self.running_times]
            flat_list = [item for sublist in l for item in sublist]
            makespan_minimization += sum(flat_list) <= c_max

        makespan_minimization.solve(pulp.PULP_CBC_CMD(timeLimit=600, msg=False))

        return makespan_minimization


if __name__ == '__main__':
    running_times = {'a': [(1, 6), (2, 5), (4, 3)], 'b': [(1, 5), (2, 2), (4, 1)], 'c': [(1, 4), (2, 1.5), (4, 0.5)],
                     'd': [(1, 8), (2, 4), (4, 2)]}
    num_jobs = len(running_times.keys())
    num_machines = 4

    lp = LinearProgram(running_times, num_machines)
    print(lp.start())
