import subprocess


class Executor:
    # program: e.g. 'sleep 10' or './myocyte.out 700 100 1 16'
    def __init__(self, job, core_mapping):
        core_assignment = ','.join([str(x) for x in job.threads])
        if core_mapping:
            self.command = "numactl --physcpubind=%s %s" % (core_assignment, job.program)
        else:
            self.command = job.program

    def exec(self):
        subprocess_exec = subprocess.Popen(self.command.split(' '))
        output = subprocess_exec.communicate()[0]
        return subprocess_exec.returncode
