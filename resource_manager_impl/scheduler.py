import threading
import time

from resource_manager import ResourceManager
from utils import ScheduleEvent, ScheduleType, Job


class Scheduler:
    def __init__(self, resource_manager: ResourceManager, result_filename, schedule_core_assignments=None):
        self.resource_manager = resource_manager
        self.jobs: [Job] = []
        self.current_schedule = []
        self.start_time = time.time()
        self.result_filename = result_filename
        self.results = []
        self.schedule_core_assignment = schedule_core_assignments

    def add_job(self, job):
        self.jobs.append(job)

        if self.schedule_core_assignment:
            # do not do scheduling if there is a job core assignment given, because the assignment already determines
            # everything... also makes thread-safe
            return

        self.do_scheduling()

    def cores_available(self, job):
        return all(x in self.resource_manager.get_free_cores() for x in job.threads)

    def wait_for_job_finish_invoke_scheduling(self, schedule_event: ScheduleEvent):
        exit_code = schedule_event.job_handle.wait_for_job_finish()
        schedule_event.job.set_finishing_time(time.time())

        if exit_code != 0:
            # todo: instead of launching again, tell that it was not executed successfully
            # self.jobs.append(schedule_event.job)
            output_failure = "Job %02d exited with failure" % schedule_event.job.id
            print(output_failure, flush=True)
            self.results.append(output_failure)

        execution_time = schedule_event.job.finishing_time - schedule_event.job.launching_time
        turnaround_time = schedule_event.job.finishing_time - schedule_event.job.submit_time

        output = "Job %02d - running time: %.2f s\n" \
                 "Job %02d - turnaround time: %.2f s" % (schedule_event.job.id, execution_time,
                                                         schedule_event.job.id, turnaround_time)

        print(output, flush=True)
        self.results.append(output)

        self.do_scheduling()

    def write_to_result_file(self):
        if not self.result_filename:
            return
        with open(self.result_filename, 'w') as f:
            f.write('%s\nMakespan: %.2f s\n' % ('\n'.join(self.results), self.get_makespan()))

    def do_scheduling(self):
        if self.schedule_core_assignment:
            schedule = self.get_schedule_from_core_assignment()
        else:
            schedule = self.create_schedule_in_order()
        if len(schedule) == 0:
            # empty schedule means no further jobs (maybe just currently)
            print("Makespan: %.2f s" % self.get_makespan(), flush=True)
            self.write_to_result_file()

        for s in schedule:
            if s.type == ScheduleType.launch:
                self.jobs.remove(s.job)
                self.results.append('Starting Job %02d: %s' % (s.job.id, str(s.job.threads)))
                s.job.set_launching_time(time.time())
                handle = self.resource_manager.launch(s.job)
                s.set_job_handle(handle)

                thread = threading.Thread(target=self.wait_for_job_finish_invoke_scheduling, args=(s,))
                thread.start()

    def get_schedule_from_core_assignment(self):
        # self.schedule_core_assignment: {'0': [1, 2, 3], '1': [1, 4], '2': [2, 4]}
        #                                remove 1 from '0' and '1' and  return 1 to launch on core 0 and 1
        # self.schedule_core_assignment: {'0': [2, 3], '1': [4], '2': [2, 4]}
        #                                return 2 to launch on core 0 and 2
        # self.schedule_core_assignment: {'0': [3], '1': [], '2': [4]}
        #                                return 3 to launch on core 0 and 4 to launch on core 2

        launched_job_ids = []
        schedule = []
        free_cores = self.resource_manager.get_free_cores()

        for k in self.schedule_core_assignment:
            if len(self.schedule_core_assignment[k]) == 0:
                continue

            job_id = self.schedule_core_assignment[k][0]
            j = [job for job in self.jobs if job.id == job_id][0]

            job_launch = self.job_is_only_at_first_position(job_id) and all(item in free_cores for item in j.threads)

            if job_launch:
                schedule.append(ScheduleEvent(ScheduleType.launch, j))
                free_cores = [x for x in free_cores if x not in j.threads]
                launched_job_ids.append(job_id)
            else:
                schedule.append(ScheduleEvent(ScheduleType.wait, j))

        # remove launched job ids from schedule core assignment

        for k in self.schedule_core_assignment:
            if len(self.schedule_core_assignment[k]) == 0:
                continue
            job_id = self.schedule_core_assignment[k][0]
            if job_id in launched_job_ids:
                self.schedule_core_assignment[k].pop(0)

        self.current_schedule = schedule
        return schedule

    def job_is_only_at_first_position(self, job_id):
        for k in self.schedule_core_assignment:
            if job_id in self.schedule_core_assignment[k] and self.schedule_core_assignment[k].index(job_id) != 0:
                return False
        return True

    # creates schedule in given order, this means that job x will always be executed before job x+1,
    # even though the resources for job x+1 may already be available
    def create_schedule_in_order(self):
        schedule = []
        free_cores = self.resource_manager.get_free_cores()
        launching = True
        for j in self.jobs:
            if not launching:
                schedule.append(ScheduleEvent(ScheduleType.wait, j))
            elif all(item in free_cores for item in j.threads):
                schedule.append(ScheduleEvent(ScheduleType.launch, j))
                free_cores = [x for x in free_cores if x not in j.threads]
            else:
                schedule.append(ScheduleEvent(ScheduleType.wait, j))
                launching = False

        self.current_schedule = schedule
        return schedule

    # creates schedule in given order of jobs, but allows execution of job x+1,
    # if resources for job x are not available, but resources for job x are
    def create_schedule_priority_free_cores(self):
        schedule = []
        free_cores = self.resource_manager.get_free_cores()
        for j in self.jobs:
            if all(item in free_cores for item in j.threads):
                schedule.append(ScheduleEvent(ScheduleType.launch, j))
                free_cores = [x for x in free_cores if x not in j.threads]
            else:
                schedule.append(ScheduleEvent(ScheduleType.wait, j))

        self.current_schedule = schedule
        return schedule

    def create_schedule_smallest_requirements_first(self):
        schedule = []
        free_cores = self.resource_manager.get_free_cores()

        jobs_ordered_largest_requirement = sorted(self.jobs, key=lambda x: len(x.threads))

        for j in jobs_ordered_largest_requirement:
            if all(item in free_cores for item in j.threads):
                schedule.append(ScheduleEvent(ScheduleType.launch, j))
                free_cores = [x for x in free_cores if x not in j.threads]
            else:
                schedule.append(ScheduleEvent(ScheduleType.wait, j))

        self.current_schedule = schedule
        return schedule

    def get_status(self):
        jobs_in_waiting_queue = ["Job %02d" % e.job.id for e in self.current_schedule if e.type == ScheduleType.wait]
        if len(jobs_in_waiting_queue) == 0:
            return "Jobs in waiting queue: None"
        else:
            return "Jobs in waiting queue: %s" % (','.join(jobs_in_waiting_queue))

    def get_makespan(self):
        return time.time() - self.start_time
