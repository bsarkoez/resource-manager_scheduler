import time
from enum import Enum


class Job:
    def __init__(self, unique_id: int, id: int, program: str, threads: [int]):
        self.unique_id = unique_id
        self.id = id
        self.program = program
        self.threads = threads
        self.exit_code = None
        self.submit_time = time.time()
        self.launching_time = None
        self.finishing_time = None

    def __repr__(self):
        return "Job %d: %s on %s" % (self.id, self.program, str(self.threads))

    def set_exit_code(self, exit_code):
        self.exit_code = exit_code

    def set_finishing_time(self, finishing_time):
        self.finishing_time = finishing_time

    def set_launching_time(self, launching_time):
        self.launching_time = launching_time


class JobHandle:
    def __init__(self, job, executing_thread):
        self.job = job
        self.executing_thread = executing_thread

    def wait_for_job_finish(self):
        self.executing_thread.join()
        return self.job.exit_code


class ScheduleEvent:
    def __init__(self, type, job):
        self.type: ScheduleType = type
        self.job: Job = job
        self.job_handle: JobHandle = None

    def set_job_handle(self, job_handle):
        self.job_handle = job_handle


class ScheduleType(Enum):
    launch = 1
    wait = 2


class TemporaryJob:
    def __init__(self, id: int, program: str, threads: str):
        self.id = id
        self.program = program
        self.threads = threads


class UserCommandType(Enum):
    submit = 1
    status = 2
    exit = 3
    server_shutdown = 4


class UserCommand:
    def __init__(self, type: UserCommandType, job: TemporaryJob):
        self.type = type
        self.job = job


