import argparse
import sys
import time

from resource_manager import ResourceManager
from scheduler import Scheduler
from utils import Job


def parse_file_jobs(file):
    program_dict = {}
    order_list = []
    jobs_order_toggle = 'jobs'

    id_count = 0

    with open(file, 'r') as f:
        for line in f.readlines():
            if '# JOBS' in line.strip():
                continue

            if '# ORDER' in line.strip():
                jobs_order_toggle = 'order'
                continue

            if jobs_order_toggle == 'jobs':
                id_count += 1
                unique_id = str(id_count)
                id = int(line.split(';')[0])
                program = line.split(';')[1]
                threads = line.split(';')[2].strip()
                program_dict[unique_id] = (id, program, get_list_of_threads(threads))
            else:
                time_stamp = int(line.split(':')[0])
                jobs = line.split(':')[1].strip().split(',')
                order_list.append((time_stamp, jobs))

    return program_dict, sorted(order_list, key=lambda x: x[0])


def get_list_of_threads(threads_str):
    if '-' in threads_str:
        start = int(threads_str.split('-')[0])
        count = 1

        if ':' in threads_str:
            end = int(threads_str.split('-')[1].split(':')[0])
            count = int(threads_str.split(':')[1])
        else:
            end = int(threads_str.split('-')[1])

        return list(range(start, end + 1, count))
    else:
        # enumeration with ,
        return list(map(int, threads_str.split(',')))


def argument_handling():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cores', help='number of cores of the machine', type=int, required=True)
    parser.add_argument('-f', '--file', help='filename of file containing launching order', required=True)
    parser.add_argument('-m', '--map', help='map cores, i.e., mapping not given by command', required=False,
                        action='store_true')

    args = parser.parse_args(sys.argv[1:])

    return args.map, args.cores, args.file


def read_in_and_launch(job_launch_file):
    program_dict, order_list = parse_file_jobs(job_launch_file)
    prev_time_stamp = 0

    for o in order_list:
        current_time_stamp = o[0]
        jobs_to_launch = o[1]

        time.sleep(current_time_stamp - prev_time_stamp)
        prev_time_stamp = current_time_stamp

        for j in jobs_to_launch:
            scheduler.add_job(Job(int(j), program_dict[j][0], program_dict[j][1], program_dict[j][2]))


if __name__ == '__main__':
    core_mapping, cores, filename = argument_handling()

    result_filename = 'results'
    if 'launching_order_' in filename:
        result_filename = 'results/results_%s' % filename.split('launching_order_')[1]

    resource_manager = ResourceManager(cores, core_mapping)
    scheduler = Scheduler(resource_manager, result_filename)

    read_in_and_launch(filename)
