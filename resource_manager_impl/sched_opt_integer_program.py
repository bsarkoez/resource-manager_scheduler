# Python version of sched_opt.jl
# Original author: Sascha Hunold <sascha.hunold@tuwien.ac.at>
import pulp


class SchedOpt:
    def __init__(self, running_times_dict, cores):
        self.running_times = running_times_dict
        self.cores = cores

    def start(self):
        m = self.get_ip_solution()
        variables = [v for v in m.variables() if v.varValue != 0]
        starting_times = [v for v in m.variables() if v.name.startswith('job_stime_')]

        cmax = [float(v.varValue) for v in variables if v.name == 'cmax'][0]

        job_nproc_variables = [v.name for v in variables if v.name.startswith('job_nproc_')]
        job_nproc_solution = [(x.split('_')[2], int(x.split('_')[3])) for x in job_nproc_variables]

        # job_core_assignment: [(job_id, number of cores, core assignment, running_time), ...]
        job_core_assignment = []

        for (job_id, num_cores) in job_nproc_solution:
            cores = [int(v.name.split('job_on_mach_')[1].split('_')[1])
                     for v in variables
                     if v.name.startswith('job_on_mach') and v.name.split('job_on_mach_')[1].split('_')[0] == job_id]

            if len(cores) != num_cores:
                print("ERROR: assigned cores doesn't correspond to number of cores")
                exit(1)

            running_time = [t for (x, t) in self.running_times[job_id] if x == num_cores][0]
            starting_time_job = [x.varValue for x in starting_times if x.name.split('_')[-1] == job_id][0]
            job_core_assignment.append((job_id, num_cores, cores, running_time, starting_time_job))

        job_core_assignment = sorted(job_core_assignment, key=lambda t: t[4])

        schedule = {}
        M = max(self.cores)
        for i in range(M):
            schedule[str(i)] = []

        for job_id, _, core_list, _, _ in job_core_assignment:
            for c in core_list:
                schedule[str(c)].append(job_id)

        for i in range(M):
            if len(schedule[str(i)]) <= 1:
                # no order
                continue
            starting_times_jobs_this_machine = [(x.name.split('_')[-1], x.varValue)
                                                for x in starting_times if x.name.split('_')[-1] in schedule[str(i)]]
            starting_times_job_sorted = [job_id for (job_id, _)
                                         in sorted(starting_times_jobs_this_machine, key=lambda t: t[1])]
            schedule[str(i)] = starting_times_job_sorted

        return cmax, job_core_assignment, schedule

    def get_ip_solution(self):
        M = max(self.cores)
        all_running_times = [item for sublist in list(self.running_times.values()) for item in sublist]
        sequential_running_times = [i for (c, i) in all_running_times if c == 1]
        LARGE = sum(sequential_running_times)

        m = pulp.LpProblem("sched opt", pulp.LpMinimize)

        # variables

        job_on_mach_var = ['%s-%d' % (job_id, mach) for job_id in self.running_times for mach in range(M)]
        job_on_mach = pulp.LpVariable.dicts('job_on_mach', job_on_mach_var, cat='Binary')

        job_nproc_var = ['%s-%d' % (job_id, nproc) for job_id in self.running_times for nproc in self.cores]
        job_nproc = pulp.LpVariable.dicts('job_nproc', job_nproc_var, cat='Binary')

        job_before_job_var = ['%s-%s-%d' % (job_id, job_id_1, mach)
                              for job_id in self.running_times
                              for job_id_1 in self.running_times
                              for mach in range(M)]
        job_before_job = pulp.LpVariable.dicts('job_before_job', job_before_job_var, cat='Binary')

        job_names = ['%s' % job_id for job_id in self.running_times]
        job_stime = pulp.LpVariable.dicts('job_stime', job_names, lowBound=0, cat='Continuous')
        job_ctime = pulp.LpVariable.dicts('job_ctime', job_names, lowBound=0, cat='Continuous')

        cmax = pulp.LpVariable('cmax', lowBound=0, cat='Continuous')
        # objective
        m += cmax, 'Z'

        # constraints

        for i in self.running_times:
            m += sum(job_nproc['%s-%d' % (i, nproc)] for nproc in self.cores) == 1

            m += sum(job_on_mach['%s-%d' % (i, mach)] for mach in range(M)) == sum(
                nproc * job_nproc['%s-%d' % (i, nproc)] for nproc in self.cores)

            m += job_stime[i] + sum(job_nproc['%s-%d' % (i, nproc)] *
                                    [r for (c, r) in self.running_times[i] if c == nproc][0]
                                    for nproc in self.cores) == job_ctime[i]

            m += job_ctime[i] <= cmax

        for i in self.running_times:
            for k in self.running_times:
                for j in range(M):
                    if i == k:
                        m += job_before_job['%s-%s-%d' % (i, k, j)] == 0
                    else:
                        m += job_before_job['%s-%s-%d' % (i, k, j)] + job_before_job['%s-%s-%d' % (k, i, j)] \
                             >= job_on_mach['%s-%d' % (i, j)] + job_on_mach['%s-%d' % (k, j)] - 1

                        m += job_before_job['%s-%s-%d' % (i, k, j)] + job_before_job['%s-%s-%d' % (k, i, j)] <= 1

                        m += job_ctime[i] <= job_stime[k] + LARGE - job_before_job['%s-%s-%d' % (i, k, j)] * LARGE

        m.solve(pulp.PULP_CBC_CMD(timeLimit=600, msg=False))

        return m


if __name__ == '__main__':
    # cores = [1, 2, 4]
    # running_times = {'a': [(1, 6), (2, 5), (4, 3)], 'b': [(1, 5), (2, 2), (4, 1)],
    #                  'c': [(1, 4), (2, 1.5), (4, 0.5)], 'd': [(1, 8), (2, 4), (4, 2)],
    #                  'e': [(1, 7), (2, 6), (4, 5)], 'f': [(1, 4), (2, 4), (4, 3)]
    #                  }

    cores = [1, 2]
    running_times = {'a': [(1, 6), (2, 5)], 'b': [(1, 5), (2, 2)],
                     'c': [(1, 4), (2, 1.5)], 'd': [(1, 8), (2, 4)],
                     'e': [(1, 7), (2, 6)], 'f': [(1, 4), (2, 4)]
                     }

    ip = SchedOpt(running_times, cores)
    cmax, job_core_assignment, schedule = ip.start()
    print(job_core_assignment)
    print(schedule)







