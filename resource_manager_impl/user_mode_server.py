import argparse
import sys
import threading
from server import Server


# def handle_signal():
#     server.close()
#     sys.exit()

def argument_handling():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cores', help='number of cores of the machine', required=True)

    args = parser.parse_args(sys.argv[1:])

    return int(args.cores)


if __name__ == '__main__':
    cores = argument_handling()

    server = Server(cores)

    server_thread = threading.Thread(target=server.start)
    server_thread.start()

    # signal.signal(signal.SIGINT, handle_signal)
