import argparse
from client import Client
from utils import TemporaryJob, UserCommand, UserCommandType


def read_input_handle_command():
    user_input = input("> ")
    command = user_input.split(' ')[0]

    if command not in ['submit', 'status', 'exit', 'server-shutdown']:
        print('Usage: <submit> | <status> | <exit> | <server-shutdown>')
        return None

    if command in ['status', 'exit', 'server-shutdown'] and command != user_input:
        print('Usage: <status>, <exit>, and <server-shutdown> do not have any arguments')
        return None

    if command == 'status':
        return UserCommand(UserCommandType.status, None)

    if command == 'exit':
        return UserCommand(UserCommandType.exit, None)

    if command == 'server-shutdown':
        return UserCommand(UserCommandType.server_shutdown, None)

    if command == 'submit':
        parser = argparse.ArgumentParser()
        parser.add_argument('-j', '--job', help='job to submit', required=True, nargs='+')
        parser.add_argument('-a', '--assignment', help='core assignment', required=True)

        arguments = user_input.split(' ')
        try:
            args, unknown = parser.parse_known_args(arguments[1:])
        except SystemExit:
            return None

        program = ' '.join(args.job + unknown)
        cores = args.core_assignment

        j = TemporaryJob(-1, program, cores)

        return UserCommand(UserCommandType.submit, j)


if __name__ == '__main__':
    total_jobs = 0

    while True:
        command = read_input_handle_command()
        if not command:
            continue

        client = Client()

        if command.type == UserCommandType.exit:
            client.send_msg('exit')
            break

        if command.type == UserCommandType.status:
            client.send_msg('status')

        if command.type == UserCommandType.server_shutdown:
            client.send_msg('server-shutdown')
            break

        if command.type == UserCommandType.submit:
            print(command.job.id, command.job.program, command.job.threads)

            client.send_msg('submit [%d;%s;%s]' % (command.job.id, command.job.program, command.job.threads))
