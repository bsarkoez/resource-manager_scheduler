

# Copyright (C) 2021
# Sascha Hunold <sascha.hunold@tuwien.ac.at>

using JuMP
using Gurobi
#using GLPK
#using Printf


function solve_p_procj_cmax(p, num_procs)  

    N = size(p)[1]
    M = maximum(num_procs)
    NC = length(num_procs) # how many configurations do we have
    #LARGE = 1e6
    # sum sequential running times (max cmax)
    LARGE = sum(p[:,1])
    
    
    #m = Model(with_optimizer(Gurobi.Optimizer, OutputFlag=0))
    m = Model(with_optimizer(Gurobi.Optimizer, OutputFlag=0, IntFeasTol=1e-9))
    #m = Model(with_optimizer(GLPK.Optimizer))
    
    
    @variable(m, job_on_mach[1:N,1:M], Bin )

    @variable(m, job_nproc[1:N, 1:NC], Bin)

    #@variable(m, job_before_job[1:N, 1:N, 1:M], Bin)
    @variable(m, job_before_job[1:N, 1:N, 1:M], Bin)

    @variable(m, job_stime[1:N] >=0 )
    @variable(m, job_ctime[1:N] >=0 )

    @variable(m, cmax >= 0 )
    @objective(m, Min, cmax)
    #@objective(m, Min, cmax)


    for i = 1:N
        # job needs to be in one configuration (nb of procs needs to be fixed)
        @constraint(m, sum(job_nproc[i,j] for j = 1:NC) == 1 )
        #@constraint(m, job_nproc[i,1] == 1 ) # test
    end

    # make sure that job is on as many machines as given in the configuration
    for i = 1:N
        @constraint(m, sum(job_on_mach[i,j] for j = 1:M) == sum(num_procs[j] * job_nproc[i,j] for j = 1:NC) )
    end
 
    for i = 1:N        
        @constraint(m, job_stime[i] + sum(job_nproc[i,j] * p[i,j] for j=1:NC) == job_ctime[i] )
    end

    for i = 1:N
        @constraint(m, job_ctime[i] <= cmax )
    end

    for i = 1:N
        for k = 1:N
            for j = 1:M
                if i == k
                    @constraint(m, job_before_job[i,k,j] == 0 )
                    #@constraint(m, job_before_job[k,i,j] == 0 )
                else
                    @constraint(m, job_before_job[i,k,j] + job_before_job[k,i,j] >=  job_on_mach[i,j] + job_on_mach[k,j] - 1 )
                    @constraint(m, job_before_job[i,k,j] + job_before_job[k,i,j] <= 1 )
                end
            end
        end                    
    end

    for i = 1:N
        for k = 1:N
            for j = 1:M
                # stime[k] >= ctime[i] if job_before_job[i,k,j] == 1
                if i != k
                    #@constraint(m, job_before_job[i,k,j] * job_ctime[i] <= job_stime[k] )
                    @constraint(m, job_ctime[i] <= job_stime[k] + LARGE - job_before_job[i,k,j] * LARGE )
                end
            end
        end                    
    end

 
    println("solving...")

    optimize!(m)

    cmax = objective_value(m)
    println("makespan: ", cmax)

    job_nproc      = value.(job_nproc)
    println("job_nproc\n", job_nproc)

    job_on_mach      = value.(job_on_mach)
    println("job_on_mach\n", job_on_mach)

    job_stime      = value.(job_stime)
    println("job_stime\n", job_stime)

    job_ctime      = value.(job_ctime)
    println("job_ctime\n", job_ctime)

    for i=1:N
        println("job ", i)
        print("mach : ")
        for j=1:M
            if isapprox(job_on_mach[i,j], 1, rtol=1e-5)
                print(" ", j)
            end
        end
        println()
    end

    job_before_job = value.(job_before_job)
    println()
    #println("job_before_job\n", job_before_job)
    # for i = 1:N
    #     for k = 1:N         
    #         for j = 1:M
    #            println("job ", i, " < job ", k, " on mach ", j, " ? ", job_before_job[i,k,j])
    #         end
    #     end                    
    # end

    # for i = 1:N
    #     for k = 1:N
    #         for j = 1:M
    #             if i != k
    #                 println("job ", i, " < job ", k, " on mach ", j, " ? ", job_before_job[i,k,j])
    #                 println("job_ctime[$i] <= job_stime[$k] + $LARGE - $(job_before_job[i,k,j]) * $LARGE")
    #                 println("$(job_ctime[i]) <= $(job_stime[k]) + ", LARGE - job_before_job[i,k,j] * LARGE)
    #                 println(job_ctime[i], " <= ", job_stime[k] + LARGE - job_before_job[i,k,j] * LARGE)
    #                 println("really <=? ", job_ctime[i] <= (job_stime[k] + LARGE - job_before_job[i,k,j] * LARGE ) )
    #             end
    #         end
    #     end                    
    # end


end


num_procs = [1 2 4 8 16 32]
#num_procs = [1 2 4 8]

# rows tasks
# columns processing times for each task on num_procs cores
p = [ 10 9.6 8.5 8.4 8.1 8.0 ; 
      10 8 6 5 4.3 3.1 ; 
      13 11 10 4 2.3 2.0 ;
      14 9.5 9.4 9.1 5.0 4.9 ;
      14 9.5 9.4 9.1 2.0 1.2 ;
      14 9.5 9.4 9.1 2.0 1.2 ]

solve_p_procj_cmax(p, num_procs)

