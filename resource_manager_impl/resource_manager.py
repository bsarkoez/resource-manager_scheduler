import threading
from executor import Executor
from utils import JobHandle, Job


class ResourceManager:
    def __init__(self, machine_cores, core_mapping=False):
        self.total_cores = machine_cores
        self.cores_table = {}
        self.fill_hash_table_cores()
        self.allocation_lock = threading.Lock()
        self.core_mapping = core_mapping
        print(self.core_mapping)

    def fill_hash_table_cores(self):
        for i in range(self.total_cores):
            self.cores_table[i] = (0, None)

    def execute_program(self, job):
        executor = Executor(job, self.core_mapping)
        return_code = executor.exec()
        print("<rm>: job %d done" % job.id, flush=True)
        job.set_exit_code(return_code)
        self.release_allocated_threads(job.threads)

    def threads_available_and_allocate(self, job: Job):
        self.allocation_lock.acquire()
        available = True
        for t in job.threads:
            if self.cores_table[t][0] == 1:
                available = False

        if available:
            for t in job.threads:
                self.cores_table[t] = (1, job.unique_id)

        self.allocation_lock.release()
        return available

    def release_allocated_threads(self, threads):
        self.allocation_lock.acquire()

        for t in threads:
            self.cores_table[t] = (0, None)

        self.allocation_lock.release()

    def launch(self, job):
        if not self.threads_available_and_allocate(job):
            return None

        print("<rm>: job %d launching..." % job.id, flush=True)

        thread = threading.Thread(target=self.execute_program, args=(job,))
        thread.start()

        job_handle = JobHandle(job, thread)
        return job_handle

    def get_free_cores(self):
        return [key for (key, value) in self.cores_table.items() if value[0] == 0]

    def get_allocated_cores(self):
        return [key for (key, value) in self.cores_table.items() if value[0] == 1]

    def get_jobs_allocated(self):
        allocated = [(key, value) for (key, value) in self.cores_table.items() if value[0] == 1]
        different_jobs_allocated = set([value[1] for (key, value) in allocated])
        jobs_list_cores = []
        for j in different_jobs_allocated:
            cores_allocated_for_job = [key for (key, value) in allocated if value[1] == j]
            jobs_list_cores.append((int(j), cores_allocated_for_job))

        return jobs_list_cores

    def get_status(self):
        num_allocated_cores = len(self.get_allocated_cores())
        str = "STATUS\nAllocated cores: %d of %d\n-------------------------\n" % (num_allocated_cores, self.total_cores)
        if num_allocated_cores == 0:
            return str

        for j in self.get_jobs_allocated():
            str += "Job %02d | cores : %s\n" % (j[0], j[1])

        return str


