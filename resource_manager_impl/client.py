import socket


class Client:
    def __init__(self):
        self.host = '127.0.0.1'
        self.port = 6789
        self.socket = None

    def send_msg(self, msg):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        self.socket.sendall(str.encode(msg))
        data = self.socket.recv(1024)
        print(data.decode(), flush=True)

        self.socket.close()
