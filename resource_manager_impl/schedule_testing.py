import argparse
import math
import sys
from enum import Enum

from linear_program import LinearProgram
from resource_manager import ResourceManager
from sched_opt_integer_program import SchedOpt
from scheduler import Scheduler
from utils import Job


class SchedulingMode(Enum):
    SEQUENTIAL = 'sequential'
    PARALLEL = 'parallel'
    INTEGER_PROGRAM = 'integer-program'
    SIMPLE = 'simple'


def argument_handling():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cores', help='number of cores of the machine', required=True)
    parser.add_argument('-f', '--file', help='filename of file containing jobs and execution times', required=True)
    parser.add_argument('-m', '--mode', help='scheduling mode: [sequential | parallel | integer-program | simple]', required=True,
                        choices=['sequential', 'parallel', 'integer-program', 'simple'])

    args = parser.parse_args(sys.argv[1:])

    return int(args.cores), args.file, SchedulingMode(args.mode)


def parse_input_file(f):
    inputs = []
    with open(f, 'r') as file:
        for l in file.readlines():
            line_parts = l.strip().split(';')
            job_id = int(line_parts[0])
            app_name = line_parts[1]
            app_command = line_parts[2]
            estimated_times = [tuple(map(float, i.replace('(', '').replace(')', '').split(',')))
                               for i in line_parts[3].split('),(')]
            inputs.append((job_id, app_name, app_command, [(int(i), j) for (i, j) in estimated_times]))
    return inputs


def get_makespan_of_schedule(schedule_list, num_cores):
    lp = LinearProgram({}, num_cores)
    starting_times_dict, jobs_with_starting_times = lp.assign_starting_times(schedule_list)
    return lp.get_cmax(starting_times_dict)


def create_simple_schedule(estimated_times, num_cores):
    seq_times = [(n, [x for (c, x) in t if c == 1][0]) for (n, t) in estimated_times.items()]
    sorted_seq_times = sorted(seq_times, key=lambda x: x[1], reverse=True)

    schedule_list = [[n, 0, [], t] for (n, t) in sorted_seq_times]

    for i in range(num_cores * math.ceil(len(sorted_seq_times) / num_cores)):
        idx = i % len(sorted_seq_times)
        schedule_list[idx][1] += 1
        schedule_list[idx][2].append(i % num_cores)

    return [(n, c, l, round(t / c, 2)) for [n, c, l, t] in schedule_list]


def create_schedule_according_to_mode(scheduling_mode, estimated_times, num_cores):
    if scheduling_mode == SchedulingMode.INTEGER_PROGRAM:
        # lp = LinearProgram(estimated_times, num_cores)
        # makespan, schedule = lp.start()
        #
        # res = [(n, c, l, t) for (n, c, l, t, _) in schedule]
        # schedule_dict = create_schedule_dict(res, cores)

        core_list = [c for (c, _) in list(estimated_times.values())[0]]
        ip = SchedOpt(estimated_times, core_list)
        makespan, schedule, schedule_dict = ip.start()
        res = [(n, c, l, t) for (n, c, l, t, _) in schedule]

        for s in schedule_dict:
            schedule_dict[s] = [int(x) for x in schedule_dict[s]]

        return makespan, res, schedule_dict

    if scheduling_mode == SchedulingMode.PARALLEL:
        core_assignment_list = []
        makespan = 0
        for p in estimated_times:
            running_time_32_cores = [t for (c, t) in estimated_times[p] if c == 32][0]
            makespan += running_time_32_cores
            core_assignment_list.append((p, num_cores, list(range(num_cores)), running_time_32_cores))

        schedule_dict = create_schedule_dict(core_assignment_list, num_cores)
        return makespan, core_assignment_list, schedule_dict

    if scheduling_mode == SchedulingMode.SIMPLE:
        schedule = create_simple_schedule(estimated_times, num_cores)
        schedule_dict = create_schedule_dict(schedule, num_cores)
        return get_makespan_of_schedule(schedule, num_cores), schedule, schedule_dict

    if scheduling_mode == SchedulingMode.SEQUENTIAL:
        core_assignment_list = []
        for i, p in enumerate(estimated_times):
            running_time_seq = [t for (c, t) in estimated_times[p] if c == 1][0]
            core_assignment_list.append((p, 1, [i % num_cores], running_time_seq))

        schedule_dict = create_schedule_dict(core_assignment_list, num_cores)
        return get_makespan_of_schedule(core_assignment_list, num_cores), core_assignment_list, schedule_dict


def write_expected_schedule_file(filename, job_core_assignment, makespan):
    with open(filename, 'w') as f:
        for (job_id_str, num_cores, core_list, running_time) in job_core_assignment:
            f.write('Starting Job %02d: %s\n' % (int(job_id_str), str(core_list)))
            f.write('Job %02d - running time: %.2f s\n' % (int(job_id_str), running_time))
        f.write('Makespan: %.2f\n' % makespan)


def create_schedule_dict(list_job_cores, num_total_cores):
    schedule_dict = {}
    for i in range(num_total_cores):
        schedule_dict[str(i)] = []

    for job, _, core_list, _ in list_job_cores:
        for c in core_list:
            schedule_dict[str(c)].append(int(job))

    return schedule_dict


if __name__ == '__main__':
    cores, filename, mode = argument_handling()
    result_filename = 'results/results_%s_%s' % (mode.name, filename.split('/')[-1])
    result_file_expected_schedule = 'results/expected_schedule_%s_%s' % (mode.name, filename.split('/')[-1])

    applications = parse_input_file(filename)
    app_dict = {str(i): r for (i, _, _, r) in applications}
    makespan, res, schedule = create_schedule_according_to_mode(mode, app_dict, cores)

    write_expected_schedule_file(result_file_expected_schedule, res, makespan)

    resource_manager = ResourceManager(cores)
    scheduler = Scheduler(resource_manager, result_filename, schedule)

    for i, j in enumerate(res):
        unique_id = i + 1
        job_id = int(j[0])
        program_str = [p for (i, _, p, _) in applications if str(i) == j[0]][0].replace('<p>', str(j[1]))
        thread_list = j[2]

        scheduler.add_job(Job(unique_id, job_id, program_str, thread_list))

    scheduler.do_scheduling()
