# Resource-Manager_Scheduler

Resource manager and scheduler implementation for master thesis

Manual mode execution:
`main.py -c <cores> -f <filename>  [-m]` , e.g. on hydra `main.py -c 32 -f launching_order`

User mode execution:
necessary to start server and client

`user_mode_server.py -c <cores>` and `user_mode_client.py`

Client waits for input: `<status> | <exit> | <server-shutdown> | <submit>`

`server-shutdown` is currently a command from the client to the server used to shutdown the server because there's again the problem of blocking input in case of having an own argument handler for the server to shutdown

The `submit` command has 2 arguments, `-j <job> -a <core assignment>`: e.g. `submit -j sleep 2 -a 0-15` or `submit -j ./lavaMD -cores 4 -boxes1d 20 -a 4,6,8,10`
